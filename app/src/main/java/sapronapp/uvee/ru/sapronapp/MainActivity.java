package sapronapp.uvee.ru.sapronapp;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonParser;
import com.prmja.http.*;
import com.digits.sdk.android.*;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;




import com.digits.sdk.android.Digits;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "u08BwBY3xNoUd9YmkiuznOtMh";
    private static final String TWITTER_SECRET = "deA9BdbHU1B0MBLCtpBWpkv6mw3g5h4xGMLjRu8eY5lyMhcTsl";

    TextView textView;
    Button loginButton;
    TextView textView2;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new Digits());
        setContentView(R.layout.activity_main);

        DigitsAuthButton digitsButton = (DigitsAuthButton) findViewById(R.id.auth_button);
        digitsButton.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                // TODO: associate the session userID with your user model
                Toast.makeText(getApplicationContext(), "Authentication successful for "
                        + phoneNumber, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(DigitsException exception) {
                Log.d("Digits", "Sign in with Digits failure", exception);
            }
        });


        textView  = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView.setText("Sapron");
        requestQueue = Volley.newRequestQueue(this);

       // AlertDialog.Builder builder = new AlertDialog.Builder(this);
       // builder.setTitle("Kal");
       // builder.setMessage("Error");
       // builder.setCancelable(true);

        loginButton = (Button) findViewById(R.id.loginButton);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.logos);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://www.freedy.ru/simple.php?wall=Казань",
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    JSONArray jsonArray = response.getJSONArray("Казань");
                                    for (int i = 0; i < jsonArray.length();i++){
                                        JSONObject wall = jsonArray.getJSONObject(i);

                                        String id = wall.getString("id");
                                        String name = wall.getString("name");
                                        String okato = wall.getString("okato");

                                        textView2.append(id+" "+name+" "+okato+ " \n");
                                    }

                                } catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }

                        },

                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("VOLLEY", "ERROR");
                                textView2.setText("Parse Error");

                                //AlertDialog dialog2 = builder.create();


                            }
                        }


                );
                requestQueue.add(jsonObjectRequest);
            }
        });

         

    }
}
