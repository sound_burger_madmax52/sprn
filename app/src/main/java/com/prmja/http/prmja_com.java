package com.prmja.http;

import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.ExecutionException;

/**
 * Created by Malek on 2/11/2016.
 */
public class prmja_com {
    public static String Get(String s ,String[] a) throws ExecutionException, InterruptedException {

        String parameters = "";

        for (int i = 0; i < a.length; i++) {
            try {
                parameters += a[i] + "=" + URLEncoder.encode(a[i + 1], "UTF-8") + "&";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }
        return new DownloadWebpageTask().execute(s+"?"+parameters,"GET","").get();
    }

    public static String Post(String s ,String[] a) throws ExecutionException, InterruptedException {

        String parameters ="";

        for(int i=0; i<a.length ;i++){
            try {
                parameters += a[i]+"=" + URLEncoder.encode(a[i+1],"UTF-8")+"&";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }


        return new DownloadWebpageTask().execute(s,"POST",parameters).get();

    }

    private static class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

// params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0] ,urls[1],urls[2] );
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        }

        private String downloadUrl(String myurl ,String method , String data) throws IOException {
            InputStream is = null;
// Only display the first 500 characters of the retrieved
// web page content.


            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod(method);
                conn.setDoInput(true);
                conn.setRequestProperty("Charset", "utf-8");
               if(method == "POST") {
                   conn.setDoOutput(true);

                   conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                   PrintWriter out = new PrintWriter(conn.getOutputStream());
                   out.print(data);
                   out.close();
               }
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                is = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = readIt(is, conn.getContentLength());
                return contentAsString;

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            reader.read(buffer);
            return new String(buffer);
        }
    }
}
